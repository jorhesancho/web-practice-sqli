import { Router } from "express";
import { pool } from "../connection/pool";
import { handler } from "../utils/handler";

export const productRouter = Router();

/**
 * Example of value to be injected: f' UNION SELECT id_site_user, CONCAT(username, ' ', user_pass), 1 FROM site_user--
 * Urlencoded: f%27%20UNION%20SELECT%20id_site_user%2C%20CONCAT%28username%2C%20%27%20%27%2C%20user_pass%29%2C%201%20FROM%20site_user--
 */
productRouter.get(
  "/sqli",
  handler(async (req, res) => {
    const searchTerm = req.query.search;

    const query = `SELECT * FROM commodity WHERE name_commodity ILIKE '%${searchTerm}%'`;
    console.log(query);

    const result = await pool.query(query);

    res.json(result.rows);
  })
);

/**
 * Use the driver's prepared statement instead
 */
productRouter.get(
  "/fixed",
  handler(async (req, res) => {
    const searchTerm = req.query.search;

    const query = `SELECT * FROM commodity WHERE name_commodity ILIKE CONCAT('%', $1::text, '%')`;

    const result = await pool.query(query, [searchTerm]);

    res.json(result.rows);
  })
);
