import * as dotenv from "dotenv";
dotenv.config();

import express from "express";
import * as path from "path";

import { errorHandler } from "./errors/error-handler";
import { pool } from "./connection/pool";
import { productRouter } from "./product-search/product-router";
import { app as boughtCommoditiesRouter } from "./user-purchases";
import { errorRouter } from "./error-based/error-based-router";

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.set("view engine", "ejs");
app.set("views", path.join(__dirname, "/views"));

app.get("/", async (req, res) => {
  res.json((await pool.query("SELECT NOW()")).rows);
});

app.use("/products", productRouter);
app.use("/error", errorRouter);

app.use(boughtCommoditiesRouter);

app.use(errorHandler());

const PORT = process.env.PORT ?? 5000;

app.listen(PORT, () => {
  console.log(`Server started on port ${PORT}`);
  console.log(`http://localhost:${PORT}`);
});
