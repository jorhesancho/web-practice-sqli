import express from "express";

export const app = express.Router();

import {pool} from "./connection/pool";

// Poorly protected endpoint

/* Time-base sqli example

*   SELECT * FROM commodity NATURAL JOIN bought_commodities WHERE id_site_user = 1
    AND CASE
        WHEN (SELECT user = 'postgres')
        THEN
            pg_sleep(3) IS NOT NULL
        ELSE
            true
    END;

  Time-based check on whether the current user is a superuser

    SELECT * FROM commodity NATURAL JOIN bought_commodities WHERE id_site_user = 1
    AND CASE
        WHEN (SELECT usesuper
              FROM pg_user
              WHERE usename = (SELECT user))
        THEN
            pg_sleep(3) IS NOT NULL
        ELSE
            true
    END;

    Time-based check on whether db has a table that starts with a 'c'
    1%20AND%20CASE%20WHEN%20(%20EXISTS(%20SELECT%201%20FROM%20information_schema.tables%20WHERE%20table_name%20LIKE%20'c%25'))%20THEN%20pg_sleep(2)%20IS%20NOT%20NULL%20ELSE%20true%20END;
*/
app.get("/bought/:id_user", async (req, res) => {
    try {
        let baseQuery = "SELECT * FROM commodity NATURAL JOIN bought_commodities WHERE id_site_user = "
            +
            req.params.id_user + ";";
        res.render("user-purchases.ejs", {
            id_user: req.params.id_user,
            commodities: (await pool.query(baseQuery)).rows
        });
    } catch (e) {
        res.status(500).send("Something went wrong, but i won't tell you what!");
    }
});

// Protected endpoint
app.get("/bought/protected/:id_user", async (req, res) => {
    if (!isNaN(parseInt(req.params.id_user))) {
        let baseQuery = "SELECT * FROM commodity NATURAL JOIN bought_commodities WHERE id_site_user = $1";
        res.render("user-purchases.ejs", {
            id_user: req.params.id_user,
            commodities: (await pool.query(baseQuery, [req.params.id_user])).rows
        });
    } else {
        res.status(404).render("error-page.ejs", {reason: {message: "Bad parameters"}})
    }
});