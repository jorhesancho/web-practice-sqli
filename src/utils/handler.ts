import { Request, Response, NextFunction } from "express";

export const handler = <T>(
  actualHandler: (req: Request, res: Response, next: NextFunction) => Promise<T>
) => {
  return (req: Request, res: Response, next: NextFunction) => {
    actualHandler(req, res, next).catch((e) => {
      next(e);
    });
  };
};
