import { Router } from "express";
import { pool } from "../connection/pool";
import { handler } from "../utils/handler";

export const errorRouter = Router();

// params:
// name=' and 1=cast((SELECT current_database()) as int) and '1'='1
// price=1
// query:
// SELECT * FROM commodity NATURAL JOIN bought_commodities WHERE name_commodity = '' and 1=cast((SELECT current_database()) as int)
// and '1'='1' AND price_commodity = 2 ORDER BY id_commodity ASC
// result throws an error with message 'error: invalid input syntax for type integer: "sqli"'
errorRouter.get(
  "/",
  handler(async (req, res) => {
    try {
      let name_commodity = req.query.name;
      let price_commodity = req.query.price;
      if (!name_commodity || !price_commodity) {
        res.render("purchases-search.ejs", {
          commodities: [],
          name_commodity,
          price_commodity,
        });

        return;
      }
      let baseQuery = `SELECT * FROM commodity NATURAL JOIN bought_commodities WHERE name_commodity = '${name_commodity}' ${
        price_commodity ? `AND price_commodity = ${price_commodity}` : ""
      } ORDER BY id_commodity ASC`;

      console.log(baseQuery);
      const result = await pool.query(baseQuery);

      res.render("purchases-search.ejs", {
        commodities: result.rows,
        name_commodity,
        price_commodity,
      });
    } catch (e) {
      res.status(500).render("error-page.ejs", { reason: { message: e } });
    }
  })
);



// params:
// name=' and 1=cast((SELECT current_database()) as int) and '1'='1
// price=1
// query:
// SELECT * FROM commodity NATURAL JOIN bought_commodities WHERE name_commodity = $1::text AND price_commodity = $2::int ORDER BY id_commodity ASC
// result don't throw an error
errorRouter.get("/protected", async (req, res) => {
    try {
      let name_commodity = req.query.name;
      let price_commodity = req.query.price;
      if (!name_commodity || !price_commodity) {
        res.render("purchases-search.ejs", {
          commodities: [],
          name_commodity,
          price_commodity,
        });

        return;
      }
      let baseQuery = `SELECT * FROM commodity NATURAL JOIN bought_commodities WHERE name_commodity = $1::text ${
        price_commodity ? `AND price_commodity = $2::int` : ""
      } ORDER BY id_commodity ASC`;

      console.log(baseQuery);
      const result = await pool.query(baseQuery, [name_commodity, price_commodity]);
      console.log(result.rows);

      res.render("purchases-search.ejs", {
        commodities: result.rows,
        name_commodity,
        price_commodity,
      });
    } catch (e) {
      res.status(500).render("error-page.ejs", { reason: { message: "Something bad happended" } });
    }
})
