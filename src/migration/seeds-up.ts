import { pool } from "../connection/pool";

const run = async () => {
  await pool.query(
    `INSERT INTO commodity (name_commodity, price_commodity) VALUES ('Beer', 10), ('Bread', 12)`
  );

  await pool.query(
    `INSERT INTO site_user (username, user_pass) VALUES ('user', 'userpass'), ('admin', 'adminpass')`
  );
};

run();
