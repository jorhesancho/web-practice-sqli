### SQL Injection

This is our app to show the most popular types of SQL injection attacks.


To run the app, you need to have [Docker](https://www.docker.com/) installed.

Up database:
```bash
docker-compose up -d
```

Start the app:
```bash
npm run start:dev
```

Down database:
```bash
docker-compose down
```

Enter database:
```bash
docker-compose exec postgres psql -U sqli -W sqli
```
