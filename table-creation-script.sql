CREATE TABLE IF NOT EXISTS site_user (
  id_site_user SERIAL NOT NULL,
  username VARCHAR(60) NOT NULL UNIQUE,
  user_pass VARCHAR(255) NOT NULL,
  PRIMARY KEY (id_site_user));

CREATE TABLE IF NOT EXISTS commodity (
  id_commodity SERIAL NOT NULL,
  name_commodity VARCHAR(255) NOT NULL,
  price_commodity DECIMAL NOT NULL,
  PRIMARY KEY (id_commodity));

CREATE TABLE IF NOT EXISTS bought_commodities (
  id_site_user INT NOT NULL,
  id_commodity INT NOT NULL,
  PRIMARY KEY (id_commodity, id_site_user),
  CONSTRAINT site_user_fk
    FOREIGN KEY (id_site_user)
    REFERENCES site_user (id_site_user)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT commodity_fk
    FOREIGN KEY (id_commodity)
    REFERENCES commodity (id_commodity)
    ON DELETE CASCADE
    ON UPDATE CASCADE);
